#!/usr/bin/python
import shelve
from subprocess import call
import os
import os.path

from apiclient.discovery import build
from apiclient.errors import HttpError

from enviroments import *

from y2s.modelos import DataBase, Channel, Video

# enviroments.py possui as seguintes constantes
#DEVELOPER_KEY = '-- coloque sua developer key'
#YOUTUBE_API_SERVICE_NAME = "youtube"
#YOUTUBE_API_VERSION = "v3"
#FOLDER_DOWNLOAD = 'coloque o caminho da passta'

list_channels_id = ['UCu0BWk9HqOI7iqxLCxY2h8A','UCFLOjG9Q8DNBGBgWivljBbw',
    'UCQe5_872pzm_ZTOgebq1qpw']

def youtube_search(channel):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
        developerKey=DEVELOPER_KEY)

    parameters = dict(part="id,snippet",
        maxResults=10,
        order='date',
        type='video',
        channelId=channel.channelId)

    search_response = youtube.search().list(**parameters).execute()

    videos = []
  
    for search_result in search_response.get("items", []):
        videos.append( Video(search_result["id"]["videoId"], search_result["snippet"]["title"]) )
    return videos


def extractMp3(video):
    call(['youtube-dl','--extract-audio','--audio-format','mp3',video.videoId])

def main():
    database = DataBase(os.path.join('y2s','y2s.db'))
    channels = database.listChannels()
    try:
        videosToDownload = []
        for channel in channels:
            videos = youtube_search(channel)
            for video in videos:
                q = input('Deseja baixar {}? '.format(video))
                if 'S' in q.upper():
                    videosToDownload.append(video)
        # extrair mp3
        if len(videosToDownload) > 0:
            os.chdir(os.path.normpath(FOLDER_DOWNLOAD))
            for v in videosToDownload:
                extractMp3(v)
        # videos baixados com sucesso
        if len(videosToDownload) > 0:
            print('Videos baixados com sucesso.')

    except HttpError as e:
        print("An HTTP error %d occurred:\n%s" % (e.resp.status, e.content))
    #d.close()

if __name__ == "__main__":
    main()