from y2s import modelos
import unittest

class TestChannels(unittest.TestCase):

	def setUp(self):
		self.database = modelos.DataBase('database_test.db')

	def tearDown(self):
		self.database.removeChannels()
		self.database.close()

	def test_lista_canais_vazio(self):
		assert len(self.database.listChannels()) == 0

	def test_lista_canais_um_elemento(self):
		self.database.removeChannels()
		self.database.addChannel(modelos.Channel('1'))
		assert len(self.database.listChannels()) == 1
		d = self.database.listChannels()[0]
		assert self.database.listChannels()[0].channelId == d.channelId