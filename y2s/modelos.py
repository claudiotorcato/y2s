import shelve

from werkzeug.local import LocalProxy
from flask import g

from y2s import app

class DataBase():
    def __init__(self, arquivo):
        self.d = shelve.open(arquivo)

    def close(self):
        self.d.close()

    def addChannel(self, channel):
    	try:
    		temp = self.d['channels']
    	except KeyError:
    		self.d['channels'] = []
    		temp = []
    	temp.append(channel)
    	self.d['channels'] = temp

    def listChannels(self):
    	try:
    		return self.d['channels']
    	except KeyError:
    		return []

    def removeChannels(self):
    	self.d['channels'] = []

    def removeChannel(channel):
    	# TODO terminar de implementar com sobrescrita de metodos
    	try:
    		temp = self.d['channels']
    		temp.pop(channel)
    	except KeyError:
    		self.d['channels'] = []
    		temp = []
    	self.d['channels'] = temp

class Channel:
    def __init__(self, channelId):
        self.channelId = channelId

class Video:
    def __init__(self, videoId, description):
        self.videoId = videoId
        self.description = description
    def __repr__(self):
        return '{} ({})'.format(self.description, self.videoId)


def get_db():
	db = getattr(g, '_database', None)
	if db is None:
		db = g._database = DataBase(app.config['DATABASE'])
	return db

@app.teardown_appcontext
def teardown_db(exception):
	db = getattr(g, '_database', None)
	if db is not None:
		db.close()

#db = LocalProxy(get_db)
