from apiclient.discovery import build
from apiclient.errors import HttpError

from y2s.models import Video

from y2s.enviroments import *

from datetime import datetime


def youtube_search(channel):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
        developerKey=DEVELOPER_KEY)

    parameters = dict(part="id,snippet",
        maxResults=10,
        order='date',
        publishedAfter = channel.data_ultima_consulta.isoformat('T') + 'Z',
        type='video',
        channelId=channel.id_unico)

    search_response = youtube.search().list(**parameters).execute()

    videos = []
  
    for search_result in search_response.get("items", []):
        videos.append( Video(search_result["id"]["videoId"], search_result["snippet"]["title"], channel.id) )
    return videos

def youtube_channel(channel):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
        developerKey=DEVELOPER_KEY)

    parameters = dict(part="id,snippet",
        id=channel.id_unico)

    search_response = youtube.channels().list(**parameters).execute()

    titles = []

    for search_result in search_response.get("items", []):
        titles.append( search_result['snippet']['title'] )

    return titles[0]