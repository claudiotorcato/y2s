from datetime import timedelta

CELERYBEAT_SCHEDULE = {
	'procura-videos' : {
		'task' : 'y2s.list_youtube_videos',
		'schedule' : timedelta(seconds = 3600)
	}
}

CELERY_TIMEZONE = 'UTC'

CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']