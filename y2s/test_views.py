from y2s import app

import unittest

class ViewsTest(unittest.TestCase):

	def setUp(self):
		app.config.update(DATABASE = 'database_test.db')
		self.app = app.test_client()

	def tearDown(self):
		self.app.get('/reinit_channels')

	def test_empty_channels(self):
		rv = self.app.get('/')
		assert b'<td>' not in rv.data

	def test_one_channel(self):
		rv = self.app.post('/', data = dict(
				channelId = '1'
			), follow_redirects = True
		)
		rv1 = self.app.get('/')
		assert b'Listar' in rv1.data

	def test_list_videos(self):
		rv = self.app.get('/list_last_videos/UCu0BWk9HqOI7iqxLCxY2h8A')
		assert b'<td>' in rv.data