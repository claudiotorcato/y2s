from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from y2s.database import Base

from datetime import datetime

class Canal(Base):
    __tablename__ = 'canais'

    id = Column(Integer, primary_key=True)
    id_unico = Column(String(50), unique=True)
    titulo = Column(String(50), unique=True)
    data_ultima_consulta = Column(DateTime, default = datetime.now)

    videos = relationship('Video', back_populates = 'canal', cascade = 'all, delete, delete-orphan')

    def __init__(self, id_unico):
        self.id_unico = id_unico

    def __repr__(self):
        return '<Canal %r>' % (self.id_unico)

class Video(Base):
    __tablename__ = 'videos'

    id = Column(Integer, primary_key=True)
    id_unico = Column(String(50), unique=True)
    descricao = Column(String(250))
    para_download = Column(String(1), default = 'N')
    baixado = Column(String(1), default = 'N')

    id_canal = Column(Integer, ForeignKey('canais.id'))
    canal = relationship('Canal', back_populates = 'videos')

    def __init__(self, id_unico, descricao, id_canal):
        self.id_unico = id_unico
        self.descricao = descricao
        self.id_canal = id_canal

    def __repr__(self):
        return '<Video %r>' % (self.descricao)
