from flask import render_template, request, flash, redirect, url_for
from flask import g
from wtforms import Form, StringField
import sqlalchemy as sa
from y2s import app, download
from y2s.youtube import youtube_search, youtube_channel
from y2s.database import db_session
from y2s.models import Canal, Video

from datetime import datetime, timedelta

class AdicionaCanal(Form):
	canalId = StringField('Channel Id')


@app.route('/', methods = ['GET', 'POST'])
def index():
	form = AdicionaCanal(request.form)
	if request.method == 'POST' and form.validate():
		print('Oinc')
		canal = Canal(form.canalId.data)
		title = youtube_channel(canal)
		canal.titulo = title
		db_session.add(canal)
		db_session.commit()
		lista = Canal.query.all()
		flash('Canal cadastrado.')
		print('Oinc 2')
		return redirect(url_for('index'))
	lista = Canal.query.all()
	return render_template('index.html', lista=lista, form=form)

@app.route('/list_last_videos/<channelId>')
def list_last_videos(channelId):
	canal = Canal.query.filter(Canal.id == channelId).first()
	lista = Video.query.filter(Video.id_canal == canal.id)
	return render_template('list_videos.html', videos = lista, canal = canal)

@app.route('/remove_channel/<channelId>')
def remove_channel(channelId):
	canal = Canal.query.filter(Canal.id == channelId).first()
	db_session.delete(canal)
	db_session.commit()
	flash('Canal removido.')
	return redirect(url_for('index'))

@app.route('/mark_to_download/<videoId>')
def mark_to_download(videoId):
	video = Video.query.filter(Video.id == videoId).first()
	video.para_download = 'S'
	db_session.commit()
	flash('Video marcado para download.')
	return redirect(url_for('index'))	

@app.route('/extract_video/<videoId>')
def extract_video(videoId):
	result = download.delay(videoId)
	#get_results()[videoId] = result
	flash('Video sendo baixado.')
	return redirect(url_for('index'))

@app.route('/status_download')
def status_download():
	videos = Video.query.filter(Video.para_download == 'S').all()
	return render_template('status_download.html', resultados = videos)
	
@app.route('/reinit_channels')
def reinit_channels():
	Canal.query.delete()
	db_session.commit()
	flash('Canais removidos.')
	return redirect(url_for('index'))

@app.route('/create_base')
def create_base():
	from y2s.database import init_db
	init_db()
	flash('Banco recriado.')
	return redirect(url_for('index'))

@app.route('/drop_base')
def drop_base():
	from y2s.database import drop_db
	drop_db()
	flash('Tabelas dropadas.')
	return render_template('ok.html')

@app.route('/titular_canais')
def titular_canais():
	canais = Canal.query.all()
	for canal in canais:
		titulo = youtube_channel(canal)
		canal.titulo = titulo
		db_session.add(canal)
	db_session.commit()
	return render_template('ok.html')

@app.route('/mudar_datas')
def mudar_datas():
	canais = Canal.query.all()
	for canal in canais:
		canal.data_ultima_consulta = datetime.now() - timedelta(seconds = 172800)
		db_session.add(canal)
	db_session.commit()
	return render_template('ok.html')

@app.route('/remarcar/<canalId>/<int:dias>')
def remarcar(canalId, dias):
	canal = Canal.query.filter(Canal.id == canalId).first()
	canal.data_ultima_consulta = datetime.now() - timedelta(seconds = dias * 86400)
	db_session.add(canal)
	db_session.commit()
	return render_template('ok.html')