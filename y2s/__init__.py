from flask import Flask
from y2s.tasks import make_celery
from y2s.database import db_session
from y2s.models import Video, Canal
from y2s.youtube import youtube_search

from y2s.enviroments import FOLDER_DOWNLOAD

import os.path
import os
from datetime import datetime
from subprocess import call, check_output, STDOUT

app = Flask(__name__)
app.config.update(
	DATABASE = os.path.join('y2s','y2s.db'),
	SECRET_KEY=b'A*\x86\xd1%\x1c<\x84\xc7\xdb/\xe2hJ\x01\x8d\xd3Z\xd2\x94\x11%a\xa2'
)
app.config.from_envvar('Y2S', silent=True)

celery = make_celery(app)

@celery.task()
def download(videoId):
	os.chdir(os.path.normpath(FOLDER_DOWNLOAD))
	call(['youtube-dl','--extract-audio','--audio-format','mp3',videoId])
	video = Video.query.filter(Video.id_unico == videoId).first()
	video.baixado = 'S'
	db_session.commit()

@celery.task()
def list_youtube_videos():
	canais = Canal.query.all()
	for canal in canais:
		#canal.data_ultima_consulta = datetime.now()
		#db_session.add(canal)
		videos = youtube_search(canal)
		for v in videos:
			v1 = Video.query.filter(Video.id_unico == v.id_unico).first()
			if not v1:
				db_session.add(v)
	db_session.commit()	


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()



import y2s.views