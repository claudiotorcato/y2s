"""adicionar chave-estrangeira em videos para canais

Revision ID: de9fe9d3d829
Revises: 4690b27561c6
Create Date: 2016-08-25 08:09:19.239920

"""

# revision identifiers, used by Alembic.
revision = 'de9fe9d3d829'
down_revision = '4690b27561c6'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('videos', sa.Column('id_canal', sa.Integer))


def downgrade():
    op.drop_column('videos','id_canal')
