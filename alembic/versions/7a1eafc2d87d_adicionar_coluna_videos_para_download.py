"""Adicionar coluna videos.para_download

Revision ID: 7a1eafc2d87d
Revises: 949ab3eeb14c
Create Date: 2016-08-27 03:50:07.532059

"""

# revision identifiers, used by Alembic.
revision = '7a1eafc2d87d'
down_revision = '949ab3eeb14c'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('videos', sa.Column('para_download', sa.String(1)))


def downgrade():
    pass