"""criar tabela videos

Revision ID: 4690b27561c6
Revises: aa6e8ace9988
Create Date: 2016-08-25 08:05:31.121873

"""

# revision identifiers, used by Alembic.
revision = '4690b27561c6'
down_revision = 'aa6e8ace9988'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
    	'videos',
    	sa.Column('id', sa.Integer, primary_key=True),
    	sa.Column('id_unico', sa.String(50), unique=True),
    	sa.Column('descricao', sa.String(250))
    )


def downgrade():
    op.drop_table('videos')
