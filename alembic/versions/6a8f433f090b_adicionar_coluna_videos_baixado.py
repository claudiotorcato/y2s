"""Adicionar coluna videos.baixado

Revision ID: 6a8f433f090b
Revises: 7a1eafc2d87d
Create Date: 2016-08-27 04:10:47.007493

"""

# revision identifiers, used by Alembic.
revision = '6a8f433f090b'
down_revision = '7a1eafc2d87d'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('videos', sa.Column('baixado', sa.String(1)))


def downgrade():
    pass
