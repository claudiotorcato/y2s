"""Adicionar coluna canais.titulo

Revision ID: 31adab0a9199
Revises: 6a8f433f090b
Create Date: 2016-08-31 01:04:59.684251

"""

# revision identifiers, used by Alembic.
revision = '31adab0a9199'
down_revision = '6a8f433f090b'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('canais', sa.Column('titulo', sa.String(50)))


def downgrade():
    pass
