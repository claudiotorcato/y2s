"""criar tabela canais

Revision ID: aa6e8ace9988
Revises: 
Create Date: 2016-08-25 08:01:38.048542

"""

# revision identifiers, used by Alembic.
revision = 'aa6e8ace9988'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
    	'canais',
    	sa.Column('id', sa.Integer, primary_key=True),
    	sa.Column('id_unico', sa.String(50), unique=True),
    )


def downgrade():
    op.drop_table('canais')
