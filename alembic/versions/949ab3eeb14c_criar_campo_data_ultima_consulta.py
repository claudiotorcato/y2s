"""criar campo data_ultima_consulta

Revision ID: 949ab3eeb14c
Revises: de9fe9d3d829
Create Date: 2016-08-25 08:14:03.459177

"""

# revision identifiers, used by Alembic.
revision = '949ab3eeb14c'
down_revision = 'de9fe9d3d829'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('canais', sa.Column('data_ultima_consulta', sa.DateTime))


def downgrade():
    pass
